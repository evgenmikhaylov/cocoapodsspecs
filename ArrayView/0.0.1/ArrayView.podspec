Pod::Spec.new do |s|
s.name                = "ArrayView"
s.version             = "0.0.1"
s.summary             = "Custom Stack View"
s.description         = <<-DESC
                        Custom Stack View for iOS 8
                        DESC
s.homepage            = "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/arrayview"
s.license             = 'MIT'
s.author              = { "Evgeny Mikhaylov" => "evgenmikhaylov@gmail.com" }
s.source              = { :git => "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/arrayview.git", :tag => "0.0.1" }
s.platform            = :ios, '8.0'
s.requires_arc        = true
s.source_files        = 'ArrayView/*.{h,m}'

end
